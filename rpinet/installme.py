#!/usr/bin/env python3

from fire import Fire
import subprocess as sp
from subprocess import CalledProcessError, check_output
import sys

import socket

def sp_check(CMD, stop_on_fail=False):
    print("D1... ",CMD)
    print("D2... ",CMD)
    returncode = 0
    try:
        res = sp.check_output( CMD.split() )
    except FileNotFoundError as e :
        res = "not exists"
        returncode = 127
    except CalledProcessError as e:
        res = e.output
        returncode = e.returncode
    except:
        res = 'other error'
        returncode = -1
    print(f"D... check output result - {res}")
    print(returncode)
    if (returncode != 0) and (stop_on_fail):
        print("X... STOPped on FAIL")
        sys.exit(1)
    return returncode




def main():
    if sp_check("tincd --version") != 0:
        print("X... installing tinc")
        sp_check("sudo apt update", True)
        sp_check("sudo apt -y install tinc", True)

    print("i... tinc may be present - i check gpg")

    if sp_check("gpg --version") != 0:
        print("X... installing gpg")
        sp_check("sudo apt update", True)
        sp_check("sudo apt -y install gpg", True)

    print("i... tinc may be present - i check gpg keys")


    helpkeys = f"""
        x... no rpi key
        gpg --output rpi_pub.gpg --armor --export 47F47C8DD7D91DB8BBB599D8947F1D32DB0180AC
        gpg --output rpi_sec.gpg --armor --export-secret-key 47F47C8DD7D91DB8BBB599D8947F1D32DB0180AC
        gpg --import rpi_pub.gpg
        gpg --allow-secret-key-import  --import rpi_sec.gpg


        gpg --output malina_pub.gpg --armor --export 9159FE88AB2BCBDFE7A9FE8EC420E58F79D13ED0
        gpg --output malina_sec.gpg --armor --export-secret-key 9159FE88AB2BCBDFE7A9FE8EC420E58F79D13ED0
        gpg --import malina_pub.gpg
"""

    keys = sp.check_output("gpg --list-keys".split()).decode("utf8")
    print("1...",keys)
    print("2...",keys.split("\n"))

    if keys.find("9159FE88AB2BCBDFE7A9FE8EC420E58F79D13ED0")>=0:
        print("i... malina OK")
    else:
        print("x... no malina key")
        print(helpkeys)
        sys.exit(1)
    if keys.find("47F47C8DD7D91DB8BBB599D8947F1D32DB0180AC")>=0:
        print("i... rpi OK")
    else:
        print("x... no rpi key")
        print(helpkeys)

        sys.exit(1)


    print("i... i should go for config")
    res = sp.check_output( "mkdir -p /etc/tinc/rpinet".split() )

    CMD = "yes | sudo tincd -c . -K"
    res = sp.Popen(CMD,shell=True,stdout=sp.PIPE,stderr=sp.STDOUT)

    print("D... see:\n", res )

    print("-----------------config-------------")
    hostname = socket.gethostname()

    tincconf = f"""Name = {hostname}
Device = /dev/net/tun
AddressFamily = ipv4
# Interface = tun0
ConnectTo = janca20
Mode = switch
"""

    IPLAST = {}
    IPLAST["rpi3ba0f"] = 15

    IPLAST["rpi3b20a"] = 18
    IPLAST["rpi3bac2"] = 19
    IPLAST["rpi3bd87"] = 30

    IPLAST['rpi3b6b6']=20
    IPLAST['rpi3b817']=21
    IPLAST['rpi3b6d3']=22
    IPLAST['rpi3b2c6']=23

    IPLAST['rpi03a1'] = 70
    IPLAST['rpi0c01'] = 71
    IPLAST['rpi01c6'] = 72
    IPLAST['rpi0dcb'] = 73
    IPLAST['rpi05d8'] = 74
    IPLAST['rpi0d44'] = 75

    IPLAST['rpi4b456'] = 90
    IPLAST["rpi4bee1"] = 91


    XIPLAST = IPLAST[hostname]

    tdown = f"""#!/bin/sh
ip route del 10.27.0.0/16 dev $INTERFACE
ip addr del 10.27.27.{XIPLAST}/32 dev $INTERFACE
ip link set $INTERFACE down
"""
    tup = f"""#!/bin/sh
ip link set $INTERFACE up
ip addr add 10.27.27.{XIPLAST}/16 dev $INTERFACE
ip route add 10.27.0.0/16 dev $INTERFACE
"""


    with open("tinc.conf","w") as f:
        f.write(tincconf)

    with open("tinc-up","w") as f:
        f.write(tup)

    with open("tinc-down","w") as f:
        f.write(tdown)

    print("---------LAST COMMAND----")
    print(" tincd -n rpinet -D")



if __name__ == "__main__":
    Fire(main)
