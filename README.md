# This is a test repository for RPi

Hello! This all is in construction




# Appendix:
## Process with gpg keys"

### Create
 - `gpg --generate-key`

### SIGN and VERIFY
 - there are ways to sing: `gpg --sign README.md` or better  `gpg --clearsign README.md`
 - but the optimal is `gpg --armor --detach-sign README.md`
 - verify with `gpg --verify README.md.asc README.md`


### List, Upload, Search, W\Export, Revoke
  - check what is installed
  ```
  gpg --list-keys
  gpg --list-secret-keys
```
 - Upload the KEY: `gpg --send-keys --keyserver keys.gnupg.net 9159FE88AB2BCBDFE7A9FE8EC420E58F79D13ED0`

 - Search the KEY:  `gpg --keyserver keys.gnupg.net --search-keys mymail@gmail.com`

 - Import somewhere  `gpg --import public.key`

 - export own public key `gpg --export -a "mymail@gmail.com" > public.key`

 - revoking `gpg --output ~/.gnupg/revocation_.crt --gen-revoke mymail@gmail.com`
 - refresh from outside: `gpg --keyserver keys.gnupg.net  --refresh-keys`
